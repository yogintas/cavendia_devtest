import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './app/redux/store';
import App from './app/components/containers/App';
import './assets/styles/styles.scss';

ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('root')); //eslint-disable-line
