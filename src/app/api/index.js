import axios from 'axios';

const xhr = axios.create({
  baseURL: 'https://zyqh9s9xt4.execute-api.eu-west-1.amazonaws.com/prod',
  headers: { 'x-api-key': 'Akq20YpLT32lZCyj93KLY5y1jHe6zTfD8QpjNrNd' }
});

export function getBattles() {
  return xhr.get('/battle');
}

export function createBattle() {
  return xhr.post('/battle');
}

export function getBattleById(id) {
  return xhr.get(`/battle/${id}`);
}

export function deleteBattleById(id) {
  return xhr.delete(`/battle/${id}`);
}

export function fireMissileById(id, rowNum, colNum) {
  return xhr.post(`/battle/${id}/fire`, {
    coordinate: [rowNum, colNum]
  });
}

