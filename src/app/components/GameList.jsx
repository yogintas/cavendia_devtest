import React from 'react';
import uid from 'uid';
import PropTypes from 'prop-types';

const propTypes = {
  getBattle: PropTypes.func.isRequired,
  battles: PropTypes.array.isRequired
};

const generateListItems = (battles, getBattle) =>
  battles.map(({ battleId, gameStatus }) => (
    <tr onClick={getBattle.bind(null, battleId)} key={uid()}>
      <td>{ battleId }</td>
      <td>{ gameStatus }</td>
    </tr>
  ));

const GameList = ({ battles, getBattle }) => (
  <table className="sidepanel__gamelist">
    <tbody>
      <tr>
        <th>Game ID</th>
        <th>Status</th>
      </tr>
      { generateListItems(battles, getBattle) }
    </tbody>
  </table>
);

GameList.propTypes = propTypes;

export default GameList;
