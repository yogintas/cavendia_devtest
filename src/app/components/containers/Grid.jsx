import React from 'react';
import PropTypes from 'prop-types';
import uid from 'uid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as gameActions } from '../../redux/actions';
import GridRow from '../GridRow';
import Button from '../common/Button';

const propTypes = {
  fireMissile: PropTypes.func.isRequired,
  deleteBattle: PropTypes.func.isRequired,
  battleId: PropTypes.string.isRequired,
  battlefield: PropTypes.array.isRequired,
  lastEnemyHit: PropTypes.array.isRequired,
  gameStatus: PropTypes.string.isRequired
};

const defaultProps = {
  battleId: 'undefined',
  battlefield: [],
  lastEnemyHit: [],
  gameStatus: 'undefined'
};

const {
  fireMissileByIdAttempt,
  deleteBattleByIdAttempt
} = gameActions;

const generateRows = (battleId, battlefield, lastEnemyHit, fireMissile) => {
  if (battlefield) {
    return battlefield.map((cells, rowNum) => (
      <GridRow
        {...{ cells, rowNum, battleId, fireMissile, lastEnemyHit }}
        key={uid()}
      />
    ));
  }
};

const Grid = ({
  battleId,
  battlefield,
  lastEnemyHit,
  gameStatus,
  deleteBattle,
  fireMissile
}) => (
  <main>
    <div className={`grid ${gameStatus}`}>
      { generateRows(battleId, battlefield, lastEnemyHit, fireMissile) }
      <div className="grid__cover"></div>
      <Button
        className="grid__button-delete"
        value="DELETE"
        clickHandler={deleteBattle.bind(null, battleId)}
      />
    </div>
  </main>
);

Grid.propTypes = propTypes;
Grid.defaultProps = defaultProps;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fireMissile: fireMissileByIdAttempt,
    deleteBattle: deleteBattleByIdAttempt
  }, dispatch);
}

function mapStateToProps({
  game: {
    activeBattle: {
      battleId,
      battlefield,
      lastEnemyHit,
      gameStatus
    }
  }
}) {
  return {
    battleId,
    battlefield,
    lastEnemyHit,
    gameStatus
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);
