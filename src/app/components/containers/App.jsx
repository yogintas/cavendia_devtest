import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as gameActions } from '../../redux/actions';
import Grid from './Grid';
import SidePanel from './SidePanel';

const {
  getBattlesAttempt
} = gameActions;

const propTypes = {
  getBattlesAttempt: PropTypes.func.isRequired,
  attempting: PropTypes.bool.isRequired
};

class App extends Component {
  componentDidMount() {
    this.props.getBattlesAttempt();
  }

  render() {
    const {
      attempting
    } = this.props;

    return (
      <div className={`app container ${attempting ? 'loading' : ''}`}>
        <SidePanel />
        <Grid />
      </div>
    );
  }
}

App.propTypes = propTypes;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getBattlesAttempt
  }, dispatch);
}

function mapStateToProps({ game: { attempting } }) {
  return {
    attempting
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
