import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as gameActions } from '../../redux/actions';
import GameList from '../GameList';
import NewBattleButton from '../NewBattleButton';

const propTypes = {
  createBattle: PropTypes.func.isRequired,
  getBattle: PropTypes.func.isRequired,
  battles: PropTypes.array.isRequired
};

const {
  createBattleAttempt,
  getBattleByIdAttempt
} = gameActions;

const SidePanel = ({
  battles,
  createBattle,
  getBattle
}) => (
  <aside className="sidepanel">
    <GameList {...{ battles, getBattle }} />
    <NewBattleButton clickHandler={createBattle} />
  </aside>
);

SidePanel.propTypes = propTypes;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createBattle: createBattleAttempt,
    getBattle: getBattleByIdAttempt
  }, dispatch);
}

function mapStateToProps({ game: { battles } }) {
  return {
    battles,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SidePanel);

