import React from 'react';
import PropTypes from 'prop-types';
import Button from './common/Button';

const propTypes = {
  clickHandler: PropTypes.func.isRequired
};

const NewBattleButton = ({ clickHandler }) => (
  <Button
    {...{ clickHandler }}
    className="sidepanel__button-newbattle"
    value="Start New Battle"
  />
);

NewBattleButton.propTypes = propTypes;

export default NewBattleButton;
