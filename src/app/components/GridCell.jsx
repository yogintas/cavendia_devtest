import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  isLastEnemyHit: PropTypes.bool.isRequired,
  type: PropTypes.number.isRequired,
  clickHandler: PropTypes.func.isRequired
};

const GridCell = ({ isLastEnemyHit, type, clickHandler }) => (
  <div className="grid__cell-wrapper">
    <div
      className={` grid__cell type-${type} ${type !== 0 ? 'disabled' : ''} ${isLastEnemyHit ? 'lastEnemyHit' : ''}`}
      onClick={clickHandler}
      role="button"
    />
  </div>
);

GridCell.propTypes = propTypes;

export default GridCell;
