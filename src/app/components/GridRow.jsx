import React from 'react';
import uid from 'uid';
import PropTypes from 'prop-types';
import GridCell from './GridCell';

const propTypes = {
  cells: PropTypes.array.isRequired,
  battleId: PropTypes.string.isRequired,
  rowNum: PropTypes.number.isRequired,
  fireMissile: PropTypes.func.isRequired,
  lastEnemyHit: PropTypes.array.isRequired
};

const generateCells = (cells, rowNum, battleId, fireMissile, lastEnemyHit) =>
  cells.map((type, colNum) => (
    <GridCell
      isLastEnemyHit={
        lastEnemyHit
        && lastEnemyHit[0] === rowNum
        && lastEnemyHit[1] === colNum
      }
      type={type}
      clickHandler={fireMissile.bind(null, battleId, rowNum, colNum)}
      key={uid()}
    />
  ));

const GridRow = ({ cells, rowNum, battleId, fireMissile, lastEnemyHit }) => (
  <div className="grid__row">
    { generateCells(cells, rowNum, battleId, fireMissile, lastEnemyHit) }
  </div>
);

GridRow.propTypes = propTypes;

export default GridRow;
