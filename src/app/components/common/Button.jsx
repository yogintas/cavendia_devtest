import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  className: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired
};

const Button = ({ className, value, clickHandler }) => (
  <div
    role="button"
    {...{ className }}
    onClick={clickHandler}>
    { value }
  </div>
);

Button.propTypes = propTypes;

export default Button;
