import { createActions } from 'reduxsauce';

export const { Types, Creators } = createActions({
  getBattlesAttempt: [],
  getBattlesSuccess: ['data'],
  getBattlesFailure: ['err'],

  createBattleAttempt: [],
  createBattleSuccess: ['data'],
  createBattleFailure: ['err'],

  getBattleByIdAttempt: ['battleId'],
  getBattleByIdSuccess: ['data', 'battleId'],
  getBattleByIdFailure: ['err'],

  deleteBattleByIdAttempt: ['battleId'],
  deleteBattleByIdSuccess: ['battleId'],
  deleteBattleByIdFailure: ['err'],

  fireMissileByIdAttempt: ['battleId', 'rowNum', 'colNum'],
  fireMissileByIdSuccess: ['data', 'battleId'],
  fireMissileByIdFailure: ['err']
});
