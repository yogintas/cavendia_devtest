import Immutable from 'seamless-immutable';
import { createReducer } from 'reduxsauce';
import { Types } from './actions';

export const INITIAL_STATE = Immutable({
  battles: [],
  activeBattle: {
  },
  error: {},
  attempting: false
});

// GET BATTLES
const getBattlesAttempt = (state) =>
  state.merge({
    attempting: true
  });

const getBattlesSuccess = (state, { data: { battles } }) =>
  state.merge({
    battles,
    attempting: false
  });

const getBattlesFailure = (state, { error }) =>
  state.merge({
    error,
    attempting: false
  });

// CREATE BATTLE
const createBattleAttempt = (state) =>
  state.merge({
    attempting: true
  });

const createBattleSuccess = (state, { data: { battle, battleId } }) => {
  const battles = [].concat(state.battles);
  battles.push({ battleId, gameStatus: 'ONGOING' });

  return state.merge({
    battles,
    activeBattle: {
      battleId,
      ...battle
    },
    attempting: false
  });
};

const createBattleFailure = (state, { error }) =>
  state.merge({
    error,
    attempting: false
  });

// GET BATTLE BY ID
const getBattleByIdAttempt = (state) =>
  state.merge({
    attempting: true
  });

const getBattleByIdSuccess = (state, { data: { battle }, battleId }) =>
  state.merge({
    activeBattle: {
      battleId,
      ...battle
    },
    attempting: false
  });

const getBattleByIdFailure = (state, { error }) =>
  state.merge({
    error,
    attempting: false
  });

// DELETE BATTLE BY ID
const deleteBattleByIdAttempt = (state) =>
  state.merge({
    attempting: true
  });

const deleteBattleByIdSuccess = (state, { battleId }) => {
  const battles = [].concat(state.battles);
  const indexToDelete = battles.findIndex(obj => obj.battleId === battleId);
  battles.splice(indexToDelete, 1);

  return state.merge({
    activeBattle: {},
    battles,
    attempting: false
  });
};

const deleteBattleByIdFailure = (state, { error }) =>
  state.merge({
    error,
    attempting: false
  });

// FIRE MISSILE BY ID
const fireMissileByIdAttempt = (state) =>
  state.merge({
    attempting: true
  });

const fireMissileByIdSuccess = (state, {
  data: {
    battle,
    retalliationEvent: {
      coordinate
    }
  },
  battleId
}) =>
  state.merge({
    activeBattle: {
      lastEnemyHit: coordinate,
      battleId,
      ...battle
    },
    attempting: false
  });

const fireMissileByIdFailure = (state, { error }) =>
  state.merge({
    error,
    attempting: false
  });

export default createReducer(INITIAL_STATE, {
  [Types.GET_BATTLES_ATTEMPT]: getBattlesAttempt,
  [Types.GET_BATTLES_SUCCESS]: getBattlesSuccess,
  [Types.GET_BATTLES_FAILURE]: getBattlesFailure,
  [Types.CREATE_BATTLE_ATTEMPT]: createBattleAttempt,
  [Types.CREATE_BATTLE_SUCCESS]: createBattleSuccess,
  [Types.CREATE_BATTLE_FAILURE]: createBattleFailure,
  [Types.GET_BATTLE_BY_ID_ATTEMPT]: getBattleByIdAttempt,
  [Types.GET_BATTLE_BY_ID_SUCCESS]: getBattleByIdSuccess,
  [Types.GET_BATTLE_BY_ID_FAILURE]: getBattleByIdFailure,
  [Types.DELETE_BATTLE_BY_ID_ATTEMPT]: deleteBattleByIdAttempt,
  [Types.DELETE_BATTLE_BY_ID_SUCCESS]: deleteBattleByIdSuccess,
  [Types.DELETE_BATTLE_BY_ID_FAILURE]: deleteBattleByIdFailure,
  [Types.FIRE_MISSILE_BY_ID_ATTEMPT]: fireMissileByIdAttempt,
  [Types.FIRE_MISSILE_BY_ID_SUCCESS]: fireMissileByIdSuccess,
  [Types.FIRE_MISSILE_BY_ID_FAILURE]: fireMissileByIdFailure,
});
