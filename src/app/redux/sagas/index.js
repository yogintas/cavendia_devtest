import { call, put, takeEvery } from 'redux-saga/effects';
import {
  getBattles,
  createBattle,
  getBattleById,
  deleteBattleById,
  fireMissileById
} from '../../api';
import {
  Types,
  Creators as gameActions
} from '../actions';

const {
  getBattlesSuccess,
  getBattlesFailure,

  createBattleSuccess,
  createBattleFailure,

  getBattleByIdSuccess,
  getBattleByIdFailure,

  deleteBattleByIdSuccess,
  deleteBattleByIdFailure,

  fireMissileByIdSuccess,
  fireMissileByIdFailure
} = gameActions;

function* getBattlesSaga() {
  try {
    const res = yield call(getBattles);
    yield put(getBattlesSuccess(res.data));
  } catch (err) {
    yield put(getBattlesFailure(err));
  }
}

function* createBattleSaga() {
  try {
    const res = yield call(createBattle);
    yield put(createBattleSuccess(res.data));
  } catch (err) {
    yield put(createBattleFailure(err));
  }
}

function* getBattleByIdSaga({ battleId }) {
  try {
    const res = yield call(getBattleById, battleId);
    yield put(getBattleByIdSuccess(res.data, battleId));
  } catch (err) {
    yield put(getBattleByIdFailure(err));
  }
}

function* deleteBattleByIdSaga({ battleId }) {
  try {
    yield call(deleteBattleById, battleId);
    yield put(deleteBattleByIdSuccess(battleId));
  } catch (err) {
    yield put(deleteBattleByIdFailure(err));
  }
}

function* fireMissileByIdSaga({ battleId, rowNum, colNum }) {
  try {
    const res = yield call(fireMissileById, battleId, rowNum, colNum);
    yield put(fireMissileByIdSuccess(res.data, battleId));
  } catch (err) {
    yield put(fireMissileByIdFailure(err));
  }
}

function* rootSaga() {
  yield takeEvery(Types.GET_BATTLES_ATTEMPT, getBattlesSaga);
  yield takeEvery(Types.CREATE_BATTLE_ATTEMPT, createBattleSaga);
  yield takeEvery(Types.GET_BATTLE_BY_ID_ATTEMPT, getBattleByIdSaga);
  yield takeEvery(Types.DELETE_BATTLE_BY_ID_ATTEMPT, deleteBattleByIdSaga);
  yield takeEvery(Types.FIRE_MISSILE_BY_ID_ATTEMPT, fireMissileByIdSaga);
}

export default rootSaga;
