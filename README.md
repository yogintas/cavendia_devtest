**npm run dev** runs webpack-dev-server.
**npm run build-prod** compiles production bundle.
**npm run server** runs express server to serve static files from www directory.